#pragma once
#include "SDK/PluginClient.h"

struct FrameCallback {
	virtual void OnSimulationStep() = 0;
	virtual void OnRenderStep() = 0;
};

struct FrameService : Service {
	static Id GetId() {
		// {D9D3CD73-1D31-4049-AC72-DA322AB4963D}
		static const Id id =
		{ 0xd9d3cd73, 0x1d31, 0x4049,{ 0xac, 0x72, 0xda, 0x32, 0x2a, 0xb4, 0x96, 0x3d } };
		return id;
	}

	virtual void RegisterFrameCallback(FrameCallback* callback, uint16_t simStepInMillis) = 0;
	virtual void RemoveFrameCallback(FrameCallback* callback) = 0;
};