#include "SDK/PluginClient.h"
#include "SDK/FrameService.h"
#include <iostream>
#include <sstream>

#define GLEW_STATIC
#include <GL/glew.h>

void Message(std::string s);

class PluginImpl : public PluginClient, FrameCallback {
	virtual void Start(CoreApi const* coreApi) override {
		Message("Hi!\n");
		frameService = coreApi->GetService<FrameService>();
		frameService->RegisterFrameCallback(this, 16);
		glewInit();
	}

	virtual void Shutdown() override {
		Message("Goodbye, world!!\n");
		frameService->RemoveFrameCallback(this);
		frameService->Release();
	}

	virtual void OnSimulationStep() override {
	}

	virtual void OnRenderStep() override {
		glClearColor(0.1f, 0.2f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	FrameService* frameService;
};

extern "C" {
	DLL_PUBLIC PluginClient* zed_entrypoint() {
		static PluginImpl impl;
		return &impl;
	}
}

#ifdef WIN32
#include <Windows.h>
void Message(std::string s) {
	OutputDebugStringA(s.c_str());
}
#else
#include <iostream>
void Message(std::string s) {
	std::cerr << s;
}
#endif