#include "SDK/PluginClient.h"
#include "SDK/FrameService.h"
#include "PluginLoader.h"

#include <deque>
#include <functional>
#include <list>
#include <memory>
#include <sstream>
#include <string>
#include <map>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

bool reloadPlugins = true;

void on_sighup(int) {
	if (!reloadPlugins) {
		reloadPlugins = true;
	}
}

std::string BuildGameFilename(char const* base) {
	std::ostringstream oss;
#ifdef __APPLE__
	oss << "./" << base << ".dylib";
#elif defined(__linux__)
	oss << "./" << base << ".so";
#else
	char* pgmptr = {};
	_get_pgmptr(&pgmptr);
	std::string programPath = pgmptr;
	{
		auto ix = programPath.find_last_of('\\');
		programPath = programPath.substr(0, ix+1);
	}
	oss << programPath << base << ".dll";
#endif
	return oss.str();
}

struct ZedCore : CoreApi {
	virtual ServiceInfo GetServiceInfo(Id id) const override {
		ServiceInfo ret = {};
		ret.id = id;
		ret.name = "";
		return ret;
	}

	virtual bool HasService(Id id) const override {
		return !!services.count(id);
	}

	virtual Service* GetService(Id id) const override {
		auto I = services.find(id);
		return I != services.end() ? I->second.service : nullptr;
	}

	void RegisterService(Id id, Service* service) {
		ServiceEntry se = {};
		se.service = service;
		services[id] = se;
	}

	struct ServiceEntry {
		Service* service;
	};

	std::map<Id, ServiceEntry> services;
};

struct FrameRunner : FrameService {
	virtual void AddRef() {}
	virtual void Release() {}

	virtual void RegisterFrameCallback(FrameCallback* callback, uint16_t simStepInMillis) {
		callbacks.push_back(callback);
		simSteps.push_back(simStepInMillis);
	}

	virtual void RemoveFrameCallback(FrameCallback* callback) {
		auto cI = std::find(callbacks.begin(), callbacks.end(), callback);
		if (cI != callbacks.end()) {
			auto sI = simSteps.begin();
			std::advance(sI, std::distance(callbacks.begin(), cI));
			callbacks.erase(cI);
			simSteps.erase(sI);
		}
	}

	FrameRunner(ZedCore& core)
		: core(core)
	{
		core.RegisterService(GetId(), this);
	}

	void Tick(SDL_Window* window) {
		auto cB = callbacks.begin(), cI = cB, cE = callbacks.end();
		auto sB = simSteps.begin(), sI = sB, sE = simSteps.end();
		while (cI != cE) {
			(*cI)->OnSimulationStep();
			++cI;
			++sI;
		}
		glClearColor(0.9f, 0.8f, 0.7f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		cI = cB;
		sI = sB;
		while (cI != cE) {
			(*cI)->OnRenderStep();
			++cI;
			++sI;
		}
		SDL_GL_SwapWindow(window);
		SDL_Delay(10);
	}

	ZedCore& core;
	std::deque<FrameCallback*> callbacks;
	std::deque<uint16_t> simSteps;
};

int ZedMain() {
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_NOPARACHUTE);
	ZedCore core;
	FrameRunner runner(core);
	PluginLoader loader(core);

	std::string const PluginFilename = BuildGameFilename("zedgame");
	loader.AddPlugin(PluginFilename.c_str());
	std::shared_ptr<Plugin> plugin;

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 32);
	SDL_Window* mainWindow = SDL_CreateWindow("zed", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	SDL_GLContext mainContext = SDL_GL_CreateContext(mainWindow);
	SDL_GL_SetSwapInterval(1);


	bool breakLoop = false;
	while (1) {
		SDL_Event ev;
		while (SDL_PollEvent(&ev)) {
			if (ev.type == SDL_QUIT) {
				breakLoop = true;
				break;
			}
		}
		if (breakLoop) break;
		loader.Update();
		runner.Tick(mainWindow);
	}
	SDL_GL_DeleteContext(mainContext);
	SDL_DestroyWindow(mainWindow);
	SDL_Quit();
	return 0;
}

#ifdef __WIN32__
#include <Windows.h>
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	ZedMain();
}
#else
int main() {
	ZedMain();
}
#endif