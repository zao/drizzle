#include "PluginLoader.h"
#include <Windows.h>
#include <Shlobj.h>
#include <algorithm>
#include <string>
#include <vector>

struct Plugin::Module {
	HMODULE handle;
	std::string runDir;
};

static uint64_t FileSize(HANDLE fh) {
	LARGE_INTEGER cb;
	GetFileSizeEx(fh, &cb);
	return cb.QuadPart;
}

struct Directories {
	static std::string PrepareAppDir() {
		char tempDir[MAX_PATH] = {};
		HRESULT hr = SHGetFolderPathA(nullptr, CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE, nullptr, SHGFP_TYPE_CURRENT, tempDir);
		std::string ownDir = tempDir;
		ownDir += "\\zed";
		CreateDirectoryA(ownDir.c_str(), nullptr);
		return ownDir;
	}

	static std::string PrepareCacheDir() {
		std::string cacheDir = PrepareAppDir() + "\\Cache";
		CreateDirectoryA(cacheDir.c_str(), nullptr);
		return cacheDir;
	}
};

struct Filenames {

	Filenames(std::string base) {
		srcDll = base;
		srcPdb = base.substr(0, base.find_last_of('.')) + ".pdb";
		char tempPath[MAX_PATH] = {};
		auto cacheDir = Directories::PrepareCacheDir();
		{
			UUID dirId = {};
			UuidCreate(&dirId);
			RPC_CSTR s = nullptr;
			UuidToStringA(&dirId, &s);
			runDir = cacheDir + "\\run-" + (char const*)s;
			RpcStringFreeA(&s);
		}
		CreateDirectoryA(runDir.c_str(), nullptr);
		dstDll = runDir;
		dstDll += "\\" + srcDll.Basename();
		dstPdb = runDir;
		dstPdb += "\\" + std::string("syms.pdb");
	}

	struct Entry : std::string {
		Entry& operator = (std::string name) { static_cast<std::string&>(*this) = name; return *this; }
		operator char const* () const { return c_str(); }
		operator HMODULE () const { LoadLibraryA(*this); }
		Entry Basename() const {
			std::string base = substr(find_last_of('\\') + 1);
			Entry ret;
			ret = base;
			return ret;
		}

		std::vector<char> Slurp() const {
			std::vector<char> ret;
			HANDLE fh = CreateFileA(c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
			LARGE_INTEGER cb = {};
			GetFileSizeEx(fh, &cb);
			ret.resize(cb.QuadPart);
			DWORD nRead = 0;
			ReadFile(fh, ret.data(), (DWORD)ret.size(), &nRead, nullptr);
			CloseHandle(fh);
			return ret;
		}

		void Clobber(std::vector<char>&& v) const {
			HANDLE fh = CreateFileA(c_str(), GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, 0, nullptr);
			DWORD nWritten = 0;
			WriteFile(fh, v.data(), (DWORD)v.size(), &nWritten, nullptr);
			CloseHandle(fh);
		}
	};

	Entry runDir;
	Entry srcDll, srcPdb;
	Entry dstDll, dstPdb;
};

static void ObliterateDirectory(std::string path) {
	std::vector<char> name(path.size() + 2);
	std::copy(path.begin(), path.end(), name.begin());
	SHFILEOPSTRUCTA op = {};
	op.wFunc = FO_DELETE;
	op.pFrom = name.data();
	op.fFlags = FOF_NO_UI;
	SHFileOperationA(&op);
}

Plugin::Plugin(char const* filename)
	: client(nullptr)
{
	Filenames filenames(filename);
	{
		auto srcBase = filenames.srcPdb.Basename();
		auto dstBase = filenames.dstPdb.Basename();
		std::vector<char> src = filenames.srcDll.Slurp();
		std::vector<char> needle(srcBase.c_str(), srcBase.c_str() + srcBase.size() + 1);
		auto I = std::search(src.begin(), src.end(), needle.begin(), needle.end());
		std::copy_n(dstBase.c_str(), dstBase.size() + 1, I);
		filenames.dstDll.Clobber(std::move(src));
	}
	CopyFileA(filenames.srcPdb, filenames.dstPdb, FALSE);

	HMODULE h = LoadLibraryA(filenames.dstDll.c_str());
	fprintf(stderr, "Module: %p\n", h);
	if (!h) {
		DeleteFileA(filenames.dstDll);
		DeleteFileA(filenames.dstPdb);
		return;
	}

	auto entrypoint = (PluginClient::EntryPoint)GetProcAddress(h, "zed_entrypoint");
	fprintf(stderr, "Entrypoint: %p\n", entrypoint);
	if (!entrypoint) {
		FreeLibrary(h);
		DeleteFileA(filenames.dstDll);
		DeleteFileA(filenames.dstPdb);
		return;
	}

	module = std::make_shared<Module>();
	module->handle = h;
	module->runDir = filenames.runDir;
	client = entrypoint();
	fprintf(stderr, "Client: %p\n", client);
}

Plugin::~Plugin() {
	if (client) {
		client->Shutdown();
	}
	if (module) {
		if (module->handle) {
			FreeLibrary(module->handle);
		}
		if (module->runDir.size()) {
			ObliterateDirectory(module->runDir);
		}
	}
}

bool Plugin::IsLoaded() const {
	return !!client;
}

struct PluginLoader::PluginInfo {
	std::string filename;
	FILETIME modTime;
	std::shared_ptr<Plugin> instance;
};

PluginLoader::PluginLoader(CoreApi& core)
	: core(&core)
{
	auto cacheDir = Directories::PrepareCacheDir();
}

static FILETIME GetModificationTime(char const* filename) {
	FILETIME ret = {};
	HANDLE fh = CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
	if (fh != INVALID_HANDLE_VALUE) {
		GetFileTime(fh, nullptr, nullptr, &ret);
		CloseHandle(fh);
	}
	return ret;
}

void PluginLoader::AddPlugin(char const* filename) {
	auto pi = std::make_shared<PluginInfo>();
	pi->filename = filename;
	pi->modTime = GetModificationTime(filename);
	pi->instance = std::make_shared<Plugin>(filename);
	auto* c = pi->instance->GetClient();
	infos.push_back(std::move(pi));
	if (c) {
		c->Start(core);
	}
}

static bool operator > (FILETIME a, FILETIME b) {
	if (a.dwHighDateTime != b.dwHighDateTime)
		return a.dwHighDateTime > b.dwHighDateTime;
	return a.dwLowDateTime > b.dwLowDateTime;
}

void PluginLoader::Update() {
	for (auto info : infos) {
		auto newModTime = GetModificationTime(info->filename.c_str());
		if (newModTime > info->modTime) {
			info->instance.reset();
			info->instance = std::make_shared<Plugin>(info->filename.c_str());
			auto* c = info->instance->GetClient();
			if (c) {
				c->Start(core);
			}
			info->modTime = newModTime;
		}
	}
}

void* StartPluginReloadListener() {
	return nullptr;
}

void StopPluginReloadListener(void* ctx) {
}