#pragma once
#include "SDK/PluginClient.h"
#include <memory>
#include <string>
#include <vector>

struct Plugin {
	struct Module;
	explicit Plugin(char const* filename);
	~Plugin();

	bool IsLoaded() const;

	PluginClient* GetClient() { return client; }

private:
	std::shared_ptr<Module> module;
	PluginClient* client;
};

struct PluginLoader {
	struct PluginInfo;
	explicit PluginLoader(CoreApi& core);
	void AddPlugin(char const* filename);
	void Update();

private:
	CoreApi* core;
	std::vector<std::shared_ptr<PluginInfo>> infos;
};