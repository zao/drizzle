#pragma once
#include <stdint.h>
#include <cstring>

#if defined _WIN32 || defined __CYGWIN__
#ifndef BUILDING_EXE
#ifdef __GNUC__
#define DLL_PUBLIC __attribute__ ((dllexport))
#else
#define DLL_PUBLIC __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
#endif
#else
#ifdef __GNUC__
#define DLL_PUBLIC __attribute__ ((dllimport))
#else
#define DLL_PUBLIC __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
#endif
#endif
#define DLL_LOCAL
#else
#if __GNUC__ >= 4
#define DLL_PUBLIC __attribute__ ((visibility ("default")))
#define DLL_LOCAL  __attribute__ ((visibility ("hidden")))
#else
#define DLL_PUBLIC
#define DLL_LOCAL
#endif
#endif

struct Id {
	uint32_t data1;
	uint16_t data2;
	uint16_t data3;
	uint8_t data4[8];
};

static inline bool operator == (Id a, Id b) {
	return std::memcmp(&a, &b, sizeof(Id)) == 0;
}

static inline bool operator < (Id a, Id b) {
	return std::memcmp(&a, &b, sizeof(Id)) < 0;
}

class Service {
protected:
	virtual ~Service() {}
public:
	virtual void AddRef() = 0;
	virtual void Release() = 0;
};

struct ServiceInfo {
	Id id;
	char const* name;
};

class CoreApi {
public:
	virtual bool HasService(Id id) const = 0;
	virtual ServiceInfo GetServiceInfo(Id id) const = 0;
	virtual Service* GetService(Id id) const = 0;
	
	template <typename S>
	S* GetService() const {
		Service* s = GetService(S::GetId());
		return (S*)s;
	}
};

class PluginClient {
public:
	virtual ~PluginClient() {}
	virtual void Start(CoreApi const* coreApi) = 0;
	virtual void Shutdown() = 0;

	typedef PluginClient* (*EntryPoint)();
};
