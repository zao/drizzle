#include "PluginLoader.h"
#include <dlfcn.h>
#include <signal.h>

Plugin::Plugin(char const* filename)
	: module(nullptr)
	, client(nullptr)
	, started(false)
{
	module = dlopen(filename, RTLD_LAZY);
	fprintf(stderr, "Module: %p\n", module);
	if (!module) return;
	auto entrypoint = (PluginClient::EntryPoint)dlsym(module, "zed_entrypoint");
	fprintf(stderr, "Entrypoint: %p\n", entrypoint);
	if (!entrypoint) return;
	client = entrypoint();
	fprintf(stderr, "Client: %p\n", client);
}

Plugin::~Plugin() {
	if (client) {
		client->Shutdown();
	}
	if (module) {
		dlclose(module);
	}
}

void Plugin::Start() {
	if (client && !started) {
		client->Start();
	}
}

bool Plugin::IsLoaded() const {
	return client;
}

bool Plugin::IsStarted() const {
	return started;
}

void* StartPluginReloadListener() {
	{
		struct sigaction sa = {};
		sa.sa_handler = SIG_DFL;
		sigaction(SIGINT, &sa, nullptr);
		sigaction(SIGTERM, &sa, nullptr);
	}
	{
		struct sigaction sa = {};
		sa.sa_handler = on_sighup;
		sigaction(SIGHUP, &sa, nullptr);
	}
}

void StopPluginReloadListener(void* ctx) {
}